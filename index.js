let nameFasil = "Imam";
// isi variable nameStudent yg nilainya namamu
let nameStudent = "Ariawan Tampan";
// soalnya adalah reverse name variable spt = Imam JADI mamI
// petunjuknya pelajari array method = SPLIT, REVERSE, JOIN dan PUSH
// dibawah ini aku kasih flow pengerjaannya, langkah demi langkah.
// 1. string jadi array = 'imam' jadi ['imam']
// 2. looping array nya
// 3. split array = ['i', 'm', 'a', 'm'] dengan SPLIT method
// 4. di dalam lopping, reverse elemen dalam array nya dengan REVERSE method
// 5. array ke string dengan JOIN method

//Implementation code
//Function Reverse
function YourNameReverse(name) {
  var result = [];
  if (name.length === 0) {
    return "Datamu Kosong :)";
  } else {
    for (let i = name.length; i >= 0; i--) {
      result.push(name[i]);
    }
    return result.join("");
  }
}

//View result
console.log(YourNameReverse(nameStudent));
console.log(YourNameReverse(nameFasil));
